/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author Kunj Prajapati
 */
public class DiscountByPercentage extends Discount 
{

    @Override
    public double calculateDiscount(double d) {
      return amount*d/100;
    }

    public DiscountByPercentage() {
    }
    
}
    

