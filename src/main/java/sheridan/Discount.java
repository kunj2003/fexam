/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author Kunj Prajapati
 */
public abstract class Discount {
    
    public double amount;
    public double discount=50;
    
    public abstract double calculateDiscount(double d);
    
}
