/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author Kunj Prajapati
 */
public class ShoppingCartFactory {
    private static ShoppingCartFactory factory;
    
    //private default constructor --> so that  no one can instantiate from outside
    private ShoppingCartFactory()
    {}
    
    public static ShoppingCartFactory getInstance()
    {
        if(factory==null)
            factory = new ShoppingCartFactory();
        return factory;
    }
   
    public Discount getDiscount( DiscountTypes type) {
        switch ( type ) {
            case DISCOUNTBYAMOUNT : return new DiscountByAmount();
            case DISCOUNTBYPERCENTAGE : return new DiscountByPercentage();
        }
        return null;
    }
    }
        
